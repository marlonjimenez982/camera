package com.jimenez.camera;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivphoto;
    Button btcapture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivphoto=findViewById(R.id.ivphoto);
        btcapture=findViewById(R.id.btcapture);

        btcapture.setOnClickListener(this);

        String[] permisos= new String[]{

                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(permisos,1);
        }

        //----------------------------VALIDACION--------EJEMPLO 1---------------------------------
        //Hacer la actividad solo si hay permisos
        //Permisos -1 sino le han dado permisos
        // si tiene permisos tomar la foto sin boton
        /*
        if (ContextCompat.checkSelfPermission(

                this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){

            tomarFoto();
        } else {

            Toast.makeText(this, "no puedo usar la camara",Toast.LENGTH_SHORT).show();
        }
         */
        //---------------------------------------------------------------------------------------

    }

    private void tomarFoto() {

        ActivityResultLauncher<Intent> capturarFoto;
        capturarFoto= registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode()== RESULT_OK){

                            Bundle extras =result.getData().getExtras();
                            //Data contiene la imagen
                            Bitmap unaFoto= (Bitmap) extras.get("data");

                            ivphoto.setImageBitmap(unaFoto);
                        }
                    }
                });
        capturarFoto.launch(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)); //Lanzar camara

    }

    @Override
    public void onClick(View view) {

        //Falta Crear la actividadad
        /*
        if (ContextCompat.checkSelfPermission(

                this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){

            tomarFoto();
        } else {

            Toast.makeText(this, "no puedo usar la camara",Toast.LENGTH_SHORT).show();
        }
        */

    }
}